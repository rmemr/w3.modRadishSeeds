// ----------------------------------------------------------------------------
class CRadishBorderTrigger extends W3FastTravelEntity {
    // ------------------------------------------------------------------------
    event OnAreaEnter(area : CTriggerAreaComponent, activator : CComponent) {
        if (thePlayer.GetCurrentStateName() != 'RadDeactivatedPlayer') {
            super.OnAreaEnter(area, activator);
        }
    }
    // ------------------------------------------------------------------------
    event OnAreaExit(area : CTriggerAreaComponent, activator : CComponent) {
        if (thePlayer.GetCurrentStateName() != 'RadDeactivatedPlayer') {
            super.OnAreaExit(area, activator);
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
state RadDeactivatedPlayer in CPlayer {}
// ----------------------------------------------------------------------------
class CRadishModUtils extends CEntity {
    // ------------------------------------------------------------------------
    // some settings need to be tweaked while in ui mode
    // save original values
    private var hudModules: array<CName>;
    private var hudModulesEnabled: array<bool>;
    private var hoursPerMinute: float;
    // ------------------------------------------------------------------------
    private var savedPlayerPos: SRadishPlacement;
    private var savedPlayerImmortalityMode: EActorImmortalityMode;
    // ------------------------------------------------------------------------
    public function freezeTime() {
        hoursPerMinute = theGame.GetHoursPerMinute();
        theGame.SetHoursPerMinute(0);
    }
    // ------------------------------------------------------------------------
    public function unfreezeTime() {
        theGame.SetHoursPerMinute(hoursPerMinute);
    }
    // ------------------------------------------------------------------------
    public function deactivateHud() {
        var hud: CR4ScriptedHud;
        var hudModule: CR4HudModuleBase;
        var i: int;

        // collect all hud modules to disable
        hud = (CR4ScriptedHud)theGame.GetHud();
        hudModules = hud.hudModulesNames;

        for (i = 0; i < hudModules.Size(); i += 1) {
            hudModule = (CR4HudModuleBase)hud.GetHudModule(hudModules[i]);
            hudModulesEnabled.PushBack(hudModule.GetEnabled());

            // message module required for some info output
            if (hudModules[i] != 'MessageModule') {
                hudModule.SetEnabled(false);
            }
        }
    }
    // ------------------------------------------------------------------------
    public function reactivateHud() {
        var hud: CR4ScriptedHud;
        var hudModule: CR4HudModuleBase;
        var i: int;

        hud = (CR4ScriptedHud)theGame.GetHud();
        for (i = 0; i < hudModules.Size(); i += 1) {
            hudModule = (CR4HudModuleBase)hud.GetHudModule(hudModules[i]);
            hudModule.SetEnabled(hudModulesEnabled[i]);
        }
    }
    // ------------------------------------------------------------------------
    public function hidePlayer() {
        var movingAgent : CMovingAgentComponent;

        // deactivate player state changing by locking it in a custom state
        thePlayer.PushState('RadDeactivatedPlayer');
        thePlayer.LockEntryFunction(true);

        this.savedPlayerPos = SRadishPlacement(
            thePlayer.GetWorldPosition(),
            thePlayer.GetWorldRotation()
        );

        thePlayer.EnableCharacterCollisions(false);
        thePlayer.SetVisibility(false);
        thePlayer.SetTemporaryAttitudeGroup('q104_avallach_friendly_to_all', AGP_Default);

        // store previous immortality mode
        this.savedPlayerImmortalityMode = thePlayer.GetImmortalityMode();
        thePlayer.SetImmortalityMode(AIM_Unconscious, AIC_Default);

        // remove triggerchannel, add custom channel
        movingAgent = thePlayer.GetMovingAgentComponent();

        if (movingAgent) {
            // these channels need to be deactivated to prevent accidental trigger of areas
            movingAgent.RemoveTriggerActivatorChannel(TC_Default);
            movingAgent.RemoveTriggerActivatorChannel(TC_Player);
            movingAgent.RemoveTriggerActivatorChannel(TC_NPC);
            movingAgent.RemoveTriggerActivatorChannel(TC_Quest);
            movingAgent.RemoveTriggerActivatorChannel(TC_Camera);

            //TEST
            /*
            movingAgent.RemoveTriggerActivatorChannel(TC_Default);
            movingAgent.RemoveTriggerActivatorChannel(TC_Player);
            movingAgent.RemoveTriggerActivatorChannel(TC_Camera);
            movingAgent.RemoveTriggerActivatorChannel(TC_NPC);
            movingAgent.RemoveTriggerActivatorChannel(TC_SoundReverbArea);
            movingAgent.RemoveTriggerActivatorChannel(TC_SoundAmbientArea);
            movingAgent.RemoveTriggerActivatorChannel(TC_Quest);
            movingAgent.RemoveTriggerActivatorChannel(TC_Projectiles);
            movingAgent.RemoveTriggerActivatorChannel(TC_Horse);
            movingAgent.RemoveTriggerActivatorChannel(TC_Custom0);
            movingAgent.RemoveTriggerActivatorChannel(TC_Custom1);
            movingAgent.RemoveTriggerActivatorChannel(TC_Custom2);
            movingAgent.RemoveTriggerActivatorChannel(TC_Custom3);
            movingAgent.RemoveTriggerActivatorChannel(TC_Custom4);
            movingAgent.RemoveTriggerActivatorChannel(TC_Custom5);
            movingAgent.RemoveTriggerActivatorChannel(TC_Custom6);
            movingAgent.RemoveTriggerActivatorChannel(TC_Custom7);
            movingAgent.RemoveTriggerActivatorChannel(TC_Custom8);
            movingAgent.RemoveTriggerActivatorChannel(TC_Custom9);
            movingAgent.RemoveTriggerActivatorChannel(TC_Custom10);
            movingAgent.RemoveTriggerActivatorChannel(TC_Custom11);
            movingAgent.RemoveTriggerActivatorChannel(TC_Custom12);
            movingAgent.RemoveTriggerActivatorChannel(TC_Custom13);
            movingAgent.RemoveTriggerActivatorChannel(TC_Custom14);
            */

            // this channel is used for foliage trigger
            movingAgent.AddTriggerActivatorChannel(TC_Custom14);
        }
    }
    // ------------------------------------------------------------------------
    public function restorePlayer() {
        RemoveTimers();

        thePlayer.TeleportWithRotation(this.savedPlayerPos.pos, this.savedPlayerPos.rot);
        thePlayer.SetImmortalityMode(this.savedPlayerImmortalityMode, AIC_Default);

        thePlayer.ResetTemporaryAttitudeGroup(AGP_Default);
        thePlayer.EnableCharacterCollisions(true);
        thePlayer.SetVisibility(true);

        // release player state
        thePlayer.LockEntryFunction(false);
        thePlayer.PopState();

        // one time
        AddTimer('deferredRestorePlayer', 1, false, , , , true);
    }
    // ------------------------------------------------------------------------
    timer function deferredRestorePlayer(deltaTime: float, id: int) {
        var movingAgent : CMovingAgentComponent;

        // restore triggerchannel, remove custom channel
        movingAgent = thePlayer.GetMovingAgentComponent();
        if (movingAgent) {
            movingAgent.AddTriggerActivatorChannel(TC_Default);
            movingAgent.AddTriggerActivatorChannel(TC_Player);
            movingAgent.AddTriggerActivatorChannel(TC_NPC);
            //movingAgent.AddTriggerActivatorChannel(TC_SoundReverbArea);
            //movingAgent.AddTriggerActivatorChannel(TC_SoundAmbientArea);
            movingAgent.AddTriggerActivatorChannel(TC_Quest);

            movingAgent.RemoveTriggerActivatorChannel(TC_Custom14);
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
