// ----------------------------------------------------------------------------
abstract state Rad_InteractivePlacement in IScriptable {
    // ------------------------------------------------------------------------
    protected var theController: CRadishInteractivePlacement;
    protected var workContext: CName; default workContext = 'MOD_Radish_InteractivePlacement';
    // ------------------------------------------------------------------------
    protected var selectedElement: IRadishPlaceableElement;
    // ------------------------------------------------------------------------
    protected var snapToGround: bool; default snapToGround = true;
    // ------------------------------------------------------------------------
    protected var isGroundSnapable: bool; default isGroundSnapable = true;

    protected var isMoveable: bool; default isMoveable = true;
    protected var isUpDownMoveable: bool; default isUpDownMoveable = true;
    protected var isRotatableYaw: bool; default isRotatableYaw = true;
    protected var isRotatablePitch: bool; default isRotatablePitch = false;
    protected var isRotatableRoll: bool; default isRotatableRoll = false;
    private var isSwitchableRotation: bool;
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        var rotationAxis: int;
        theController = createInteractivePlacement();
        theController.startInteractiveMode(selectedElement);

        theController.activateSnapToGround(snapToGround);

        if (isRotatableYaw) rotationAxis += 1;
        if (isRotatablePitch) rotationAxis += 1;
        if (isRotatableRoll) rotationAxis += 1;

        isSwitchableRotation = rotationAxis > 1;

        theInput.StoreContext(workContext);
        registerListeners();
    }
    // ------------------------------------------------------------------------
    event OnLeaveState(nextStateName: CName) {
        theController.stopInteractiveMode();
        delete theController;

        unregisterListeners();
        theInput.RestoreContext(workContext, true);
    }
    // ------------------------------------------------------------------------
    event OnBack(action: SInputAction) {
        // immediately stop interactive movement to prevent weird behaviour with
        // view opening of previous state (no idea what the root cause is...)
        theController.stopInteractiveMode();
        backToPreviousState(action);
    }
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {
        hotkeyList.PushBack(HotkeyHelp_from('', "RAD_MoveForwardBackLeftRight", IK_MouseX, IK_MouseY));
        if (isUpDownMoveable) {
            hotkeyList.PushBack(HotkeyHelp_from('RAD_MoveUpDown'));
        }
        if (isGroundSnapable) {
            hotkeyList.PushBack(HotkeyHelp_from('RAD_ToggleSnapToGround'));
            hotkeyList.PushBack(HotkeyHelp_from('RAD_SnapToGround'));
        }
        if (isRotatableYaw || isRotatableRoll || isRotatablePitch) {
            hotkeyList.PushBack(HotkeyHelp_from('RAD_ToggleMoveRotate'));
            //TODO specialize help depending on allowed rotation axis
            hotkeyList.PushBack(HotkeyHelp_from('', "RAD_RotateYawPitch", IK_MouseX, IK_MouseY));

            if (isSwitchableRotation) {
                hotkeyList.PushBack(HotkeyHelp_from('RAD_SwitchRotateMode'));
            }
        }

        hotkeyList.PushBack(HotkeyHelp_from('RAD_ToggleFast'));
        hotkeyList.PushBack(HotkeyHelp_from('RAD_ToggleSlow'));
        hotkeyList.PushBack(HotkeyHelp_from('RAD_StopInteractivePlacement', "RAD_StopInteractivePlacement", IK_Tab, IK_LeftMouse));
    }
    // ------------------------------------------------------------------------
    event OnToggleSnapToGround(action: SInputAction) {
        var msgKey: String;
        if (isGroundSnapable && IsPressed(action)) {
            theController.activateSnapToGround(!theController.isSnapToGroundActive());

            if (theController.isSnapToGroundActive()) {
                // TODO recalculate Z to snap (cause the PhysicsCorrectZ method
                // works only near ground correctly)
                msgKey = "RAD_iSnapToGroundOn";
            } else {
                msgKey = "RAD_iSnapToGroundOff";
            }
            notice(GetLocStringByKeyExt(msgKey));
        }
    }
    // ------------------------------------------------------------------------
    event OnSnapToGround(action: SInputAction) {
        if (isGroundSnapable && IsPressed(action)) {
            theController.snapToGround();
        }
    }
    // ------------------------------------------------------------------------
    event OnToggleRotation(action: SInputAction) {
        var msgKey: String;
        if (IsPressed(action)) {
            if (!theController.isRotationMode()) {
                theController.setRotationMode(0);
                msgKey = "RAD_iRotationMode";
            } else {
                theController.activateRotationMode(false);
                msgKey = "RAD_iMovementMode";
            }
            notice(GetLocStringByKeyExt(msgKey));
        }
    }
    // ------------------------------------------------------------------------
    private function isEnabledRotationAxis(mode: int) : bool {
        switch (mode) {
            case 0: return isRotatableYaw;
            case 1: return isRotatablePitch;
            case 2: return isRotatableRoll;
            default: return false;
        }
    }
    // ------------------------------------------------------------------------
    event OnSwitchRotationMode(action: SInputAction) {
        var msgKey: String;
        var nextAxis, tries: int;

        if (IsPressed(action)) {
            // set only valid mode!
            nextAxis = (theController.getRotationMode() + 1) % 3;
            tries = 0;
            while (tries < 3 && !isEnabledRotationAxis(nextAxis)) {
                nextAxis = (nextAxis + 1) % 3;
                tries += 1;
            }

            if (tries >=3) {
                // fallback to current mode (which doesn't break anything)
                nextAxis = theController.getRotationMode();
            }
            theController.setRotationMode(nextAxis);

            switch (theController.getRotationMode()) {
                case 0: msgKey = "RAD_iRotationModeYaw"; break;
                case 1: msgKey = "RAD_iRotationModePitch"; break;
                case 2: msgKey = "RAD_iRotationModeRoll"; break;
            }
            notice(GetLocStringByKeyExt(msgKey));
        }
    }
    // ------------------------------------------------------------------------
    event OnChangeSpeed(action: SInputAction) {
        if (IsPressed(action)) {
            if (action.aName == 'RAD_ToggleFast') {
                theController.refreshConfigSettings("fast");
            } else {
                theController.refreshConfigSettings("slow");
            }
        } else if (IsReleased(action)) {
            theController.refreshConfigSettings("normal");
        }
    }
    // ------------------------------------------------------------------------
    protected function backToPreviousState(action: SInputAction);
    protected function createInteractivePlacement() : CRadishInteractivePlacement;
    protected function notice(msg: String);
    // ------------------------------------------------------------------------
    protected function registerListeners() {
        theInput.RegisterListener(this, 'OnBack', 'RAD_Back');

        theInput.RegisterListener(this, 'OnBack', 'RAD_StopInteractivePlacement');
        if (isGroundSnapable) {
            theInput.RegisterListener(this, 'OnToggleSnapToGround', 'RAD_ToggleSnapToGround');
            theInput.RegisterListener(this, 'OnSnapToGround', 'RAD_SnapToGround');
        }
        if (isRotatableYaw || isRotatableRoll || isRotatablePitch) {
            theInput.RegisterListener(this, 'OnToggleRotation', 'RAD_ToggleMoveRotate');
            if (isSwitchableRotation) {
                theInput.RegisterListener(this, 'OnSwitchRotationMode', 'RAD_SwitchRotateMode');
            }
        }
        theInput.RegisterListener(this, 'OnChangeSpeed', 'RAD_ToggleFast');
        theInput.RegisterListener(this, 'OnChangeSpeed', 'RAD_ToggleSlow');
    }
    // ------------------------------------------------------------------------
    protected function unregisterListeners() {
        theInput.UnregisterListener(this, 'RAD_Back');

        theInput.UnregisterListener(this, 'RAD_StopInteractivePlacement');
        if (isGroundSnapable) {
            theInput.UnregisterListener(this, 'RAD_ToggleSnapToGround');
            theInput.UnregisterListener(this, 'RAD_SnapToGround');
        }
        if (isRotatableYaw || isRotatableRoll || isRotatablePitch) {
            theInput.UnregisterListener(this, 'RAD_ToggleMoveRotate');

            if (isSwitchableRotation) {
                theInput.UnregisterListener(this, 'RAD_SwitchRotateMode');
            }
        }
        theInput.UnregisterListener(this, 'RAD_ToggleFast');
        theInput.UnregisterListener(this, 'RAD_ToggleSlow');
    }
    // ------------------------------------------------------------------------
    protected function createAndSetupInteractivePlacement(
        config: IRadishConfigManager, camYaw: float) : CRadishInteractivePlacement
    {
        var placement: CRadishInteractivePlacement;
        var template: CEntityTemplate;

        template = (CEntityTemplate)LoadResource("dlc\modtemplates\radishseeds\interactive_placement.w2ent", true);
        placement = (CRadishInteractivePlacement)theGame.CreateEntity(
            template, thePlayer.GetWorldPosition(), thePlayer.GetWorldRotation());

        placement.setConfig(config);
        placement.setCameraHeading(camYaw);

        return placement;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
