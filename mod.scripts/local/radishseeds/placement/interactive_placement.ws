// ----------------------------------------------------------------------------
class CRadishInteractivePlacement extends CEntity {
    // ------------------------------------------------------------------------
    protected var asset: IRadishPlaceableElement;
    protected var placement: SRadishPlacement;
    // ------------------------------------------------------------------------
    protected var stepMoveSize: float;
    protected var stepRotSize: float;
    protected var stepGamepadSize: float; default stepGamepadSize = 10.0;
    // ------------------------------------------------------------------------
    protected var configProvider: IRadishConfigManager;
    // ------------------------------------------------------------------------
    protected var isInteractiveMode: bool;
    protected var snapToGround: bool;
    protected var isRotationMode: bool;
    protected var rotationMode: int;

    protected var camHeading: float;
    protected var theWorld: CWorld;
    // ------------------------------------------------------------------------
    public function startInteractiveMode(selectedAsset: IRadishPlaceableElement)
    {
        if (!isInteractiveMode) {
            theWorld = theGame.GetWorld();

            asset = selectedAsset;
            placement = asset.getPlacement();
            asset.onPlacementStart();

            refreshConfigSettings();

            // repeats & overrideExisting = true
            AddTimer('updateInteractiveSettings', 0.015f, true, , , , true);
            isInteractiveMode = true;
        }
    }
    // ------------------------------------------------------------------------
    public function stopInteractiveMode() {
        isInteractiveMode = false;
        RemoveTimer('updateInteractiveSettings');
        asset.onPlacementEnd();
    }
    // ------------------------------------------------------------------------
    timer function updateInteractiveSettings(deltaTime: float, id: int) {
        var newPlacement: SRadishPlacement;
        var directionFB, directionLR: float;
        var moveFB, moveLR, moveUD, moveFBPad, moveLRPad: float;
        var rotateLR, rotateLRPad: float;
        var groundZ: float;

        newPlacement = placement;

        if (isRotationMode) {
            rotateLR = theInput.GetActionValue('GI_MouseDampX');

            switch (rotationMode) {
                case 0: newPlacement.rot.Yaw -= (rotateLR + rotateLRPad) * stepRotSize; break;
                case 1: newPlacement.rot.Pitch -= (rotateLR + rotateLRPad) * stepRotSize; break;
                case 2: newPlacement.rot.Roll -= (rotateLR + rotateLRPad) * stepRotSize; break;
            }

        } else {

            moveLR = theInput.GetActionValue('GI_MouseDampX');
            moveFB = - theInput.GetActionValue('GI_MouseDampY');
            moveUD = theInput.GetActionValue('RAD_MoveUpDown');

            // Gamepad Support
            moveLRPad = stepGamepadSize * theInput.GetActionValue('RAD_AxisLeftX');
            moveFBPad = stepGamepadSize * theInput.GetActionValue('RAD_AxisLeftY');
            rotateLRPad = stepGamepadSize * theInput.GetActionValue('RAD_AxisRightX');

            newPlacement.rot.Yaw -= (rotateLR + rotateLRPad) * stepRotSize;

            if (isInteractiveMode && (moveFB != 0 || moveLR != 0 || moveUD != 0 || moveFBPad != 0 || moveLRPad != 0))
            {

                // use current set cam heading so movement is aligned with visuals
                directionFB = Deg2Rad(camHeading + 90);
                directionLR = Deg2Rad(camHeading);

                newPlacement.pos.X += (moveFB + moveFBPad) * stepMoveSize * CosF(directionFB)
                                    + (moveLR + moveLRPad) * stepMoveSize * CosF(directionLR);

                newPlacement.pos.Y += (moveFB + moveFBPad) * stepMoveSize * SinF(directionFB)
                                    + (moveLR + moveLRPad) * stepMoveSize * SinF(directionLR);

                // somehow this is much slower than with mouse adjustments
                newPlacement.pos.Z += moveUD * stepMoveSize * 4;

                // deactivate snaptoGround when up/down hotkeys are used
                if (moveUD != 0) {
                    snapToGround = false;
                }

                // adjust to world surface
                if (snapToGround && theWorld.PhysicsCorrectZ(newPlacement.pos, groundZ))
                {
                    newPlacement.pos.Z = groundZ;
                }
            }
        }

        if (newPlacement != placement) {
            asset.setPlacement(newPlacement);
            placement = newPlacement;
        }
    }
    // ------------------------------------------------------------------------
    public function snapToGround() {
        var groundZ: float;

        // adjust to world surface
        if (theWorld.PhysicsCorrectZ(placement.pos, groundZ)) {
            placement.pos.Z = groundZ;
            asset.setPlacement(placement);
        }
    }
    // ------------------------------------------------------------------------
    public function isSnapToGroundActive() : bool {
        return snapToGround;
    }
    // ------------------------------------------------------------------------
    public function isRotationMode() : bool {
        return isRotationMode;
    }
    // ------------------------------------------------------------------------
    public function setCameraHeading(newHeading: float) {
        camHeading = newHeading;
    }
    // ------------------------------------------------------------------------
    public function activateSnapToGround(activate: bool) {
        snapToGround = activate;
    }
    // ------------------------------------------------------------------------
    public function activateRotationMode(activate: bool) {
        isRotationMode = activate;
        // always reset to yaw as default
        rotationMode = 0;
    }
    // ------------------------------------------------------------------------
    public function setRotationMode(mode: int) {
        isRotationMode = true;
        rotationMode = mode % 3;
    }
    // ------------------------------------------------------------------------
    public function getRotationMode() : int {
        return rotationMode;
    }
    // ------------------------------------------------------------------------
    public function setConfig(confProvider: IRadishConfigManager) {
        configProvider = confProvider;
    }
    // ------------------------------------------------------------------------
    public function refreshConfigSettings(optional mode: String) {
        var conf: SRadishPlacementConfig;

        conf = configProvider.getPlacementConfig();
        switch (mode) {
            case "slow":
                stepMoveSize = conf.slow.stepMove;
                stepRotSize = conf.slow.stepRot;
                break;

            case "fast":
                stepMoveSize = conf.fast.stepMove;
                stepRotSize = conf.fast.stepRot;
                break;

            default:
                stepMoveSize = conf.normal.stepMove;
                stepRotSize = conf.normal.stepRot;
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
