// ----------------------------------------------------------------------------
enum EEncoderValueType {
    EEVT_Null,
    EEVT_EmptyLine,
    EEVT_String,
    EEVT_Float,
    EEVT_Bool,
    EEVT_Int,
    EEVT_List,
    EEVT_Map,
}
// ----------------------------------------------------------------------------
struct SEncKeyValue {
    var k: String;
    var v: SEncValue;
}
// ----------------------------------------------------------------------------
struct SEncValue {
    var type: EEncoderValueType;
    var s: String;
    var f: Float;
    var b: bool;
    var i: int;
    // complex types:
    var l: array<SEncValue>;
    var m: array<SEncKeyValue>;
}
// ----------------------------------------------------------------------------
function KeyValueToEncKeyValue(key: String, value: SEncValue) : SEncKeyValue {
    return SEncKeyValue(StrReplaceAll(key, " ", "_"), value);
}
// ----------------------------------------------------------------------------
function IntToEncValue(i: int) : SEncValue {
    return SEncValue(EEVT_Int,,,, i);
}
// ----------------------------------------------------------------------------
function BoolToEncValue(b: bool) : SEncValue {
    return SEncValue(EEVT_Bool,,, b);
}
// ----------------------------------------------------------------------------
function FloatToEncValue(f: Float) : SEncValue {
    return SEncValue(EEVT_Float,, f);
}
// ----------------------------------------------------------------------------
function StrToEncValue(s: String) : SEncValue {
    return SEncValue(EEVT_String, StrReplaceAll(s, StrChar(92), "\\"));
}
// ----------------------------------------------------------------------------
function SeperatorToEncKeyValue() : SEncKeyValue {
    return SEncKeyValue("", SEncValue(EEVT_EmptyLine));
}
// ----------------------------------------------------------------------------
function PosToEncValue(pos: Vector) : SEncValue {
    var r: SEncValue;
    r.type = EEVT_List;
    r.l.PushBack(FloatToEncValue(pos.X));
    r.l.PushBack(FloatToEncValue(pos.Y));
    r.l.PushBack(FloatToEncValue(pos.Z));
    return r;
}
// ----------------------------------------------------------------------------
function Pos4ToEncValue(pos: Vector, optional ignoreW: bool) : SEncValue {
    var r: SEncValue;
    r.type = EEVT_List;
    r.l.PushBack(FloatToEncValue(pos.X));
    r.l.PushBack(FloatToEncValue(pos.Y));
    r.l.PushBack(FloatToEncValue(pos.Z));
    if (ignoreW) {
        r.l.PushBack(FloatToEncValue(1.0));
    } else {
        r.l.PushBack(FloatToEncValue(pos.W));
    }
    return r;
}
// ----------------------------------------------------------------------------
function RotToEncValue(rot: EulerAngles) : SEncValue {
    var r: SEncValue;
    r.type = EEVT_List;
    // expected order for radish encoders
    r.l.PushBack(FloatToEncValue(rot.Roll));
    r.l.PushBack(FloatToEncValue(rot.Pitch));
    r.l.PushBack(FloatToEncValue(rot.Yaw));
    return r;
}
// ----------------------------------------------------------------------------
function RotPosToEncValue(pos: Vector, rot: EulerAngles) : SEncValue {
    var r: SEncValue;
    r.type = EEVT_List;
    r.l.PushBack(FloatToEncValue(pos.X));
    r.l.PushBack(FloatToEncValue(pos.Y));
    r.l.PushBack(FloatToEncValue(pos.Z));
    r.l.PushBack(FloatToEncValue(rot.Yaw));
    return r;
}
// ----------------------------------------------------------------------------
function PlacementToEncValue(pos: Vector, rot: EulerAngles) : SEncValue {
    var r: SEncValue;
    r.type = EEVT_Map;
    r.m.PushBack(SEncKeyValue("pos", PosToEncValue(pos)));
    r.m.PushBack(SEncKeyValue("rot", RotToEncValue(rot)));
    return r;
}
// ----------------------------------------------------------------------------
