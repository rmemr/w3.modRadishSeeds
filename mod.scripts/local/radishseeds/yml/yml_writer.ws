// ----------------------------------------------------------------------------
// some utils
// ----------------------------------------------------------------------------
function encMapPush_str(key: String, value: String, out map: SEncValue) {
    encMapPush(key, StrToEncValue(value), map);
}
// ----------------------------------------------------------------------------
function encMapPush_int(key: String, value: int, out map: SEncValue) {
    encMapPush(key, IntToEncValue(value), map);
}
// ----------------------------------------------------------------------------
function encMapPush_float(key: String, value: Float, out map: SEncValue) {
    encMapPush(key, FloatToEncValue(value), map);
}
// ----------------------------------------------------------------------------
function encMapPush_bool(key: String, value: bool, out map: SEncValue) {
    encMapPush(key, BoolToEncValue(value), map);
}
// ----------------------------------------------------------------------------
function encListPush_str(value: String, out list: SEncValue) {
    encListPush(StrToEncValue(value), list);
}
// ----------------------------------------------------------------------------
function encListPush_int(value: int, out list: SEncValue) {
    encListPush(IntToEncValue(value), list);
}
// ----------------------------------------------------------------------------
function encListPush_float(value: Float, out list: SEncValue) {
    encListPush(FloatToEncValue(value), list);
}
// ----------------------------------------------------------------------------
function encListPush_bool(value: bool, out list: SEncValue) {
    encListPush(BoolToEncValue(value), list);
}
// ----------------------------------------------------------------------------
function encMapPush_str_opt(key: String, value: String, defaultValue: String, out map: SEncValue) {
    if (value != defaultValue) {
        encMapPush(key, StrToEncValue(value), map);
    } else {
        encMapPush("#" + key, StrToEncValue(value), map);
    }
}
// ----------------------------------------------------------------------------
function encMapPush_int_opt(key: String, value: int, defaultValue: int, out map: SEncValue) {
    if (value != defaultValue) {
        encMapPush(key, IntToEncValue(value), map);
    } else {
        encMapPush("#" + key, IntToEncValue(value), map);
    }
}
// ----------------------------------------------------------------------------
function encMapPush_float_opt(key: String, value: Float, defaultValue: Float, out map: SEncValue) {
    if (value != defaultValue) {
        encMapPush(key, FloatToEncValue(value), map);
    } else {
        encMapPush("#" + key, FloatToEncValue(value), map);
    }
}
// ----------------------------------------------------------------------------
function encMapPush_bool_opt(key: String, value: bool, defaultValue: bool, out map: SEncValue) {
    if (value != defaultValue) {
        encMapPush(key, BoolToEncValue(value), map);
    } else {
        encMapPush("#" + key, BoolToEncValue(value), map);
    }
}
// ----------------------------------------------------------------------------
function encValueNewMap() : SEncValue {
    var v: SEncValue;

    v = SEncValue(EEVT_Map);
    // this is required!
    v.m.Clear();

    return v;
}
// ----------------------------------------------------------------------------
function encValueNewList() : SEncValue {
    var v: SEncValue;

    v = SEncValue(EEVT_List);
    // this is required!
    v.l.Clear();

    return v;
}
// ----------------------------------------------------------------------------
function encMapPush(key: String, value: SEncValue, out map: SEncValue) {
    switch (value.type) {
        case EEVT_Null:     return;
        case EEVT_Map:      if (value.m.Size() == 0) { return; } break;
        case EEVT_List:     if (value.l.Size() == 0) { return; } break;
    }
    map.m.PushBack(SEncKeyValue(key, value));
}
// ----------------------------------------------------------------------------
function encListPush(value: SEncValue, out list: SEncValue) {
    switch (value.type) {
        case EEVT_Null:     return;
        case EEVT_Map:      if (value.m.Size() == 0) { return; } break;
        case EEVT_List:     if (value.l.Size() == 0) { return; } break;
    }
    list.l.PushBack(value);
}
// ----------------------------------------------------------------------------
function encMapPush_orComment(key: String, value: SEncValue, comment: String, out map: SEncValue) {
    switch (value.type) {
        case EEVT_Null:     return;
        case EEVT_Map:      if (value.m.Size() == 0) { key = "#" + key; value = StrToEncValue(comment); } break;
        case EEVT_List:     if (value.l.Size() == 0) { key = "#" + key; value = StrToEncValue(comment); } break;
    }
    map.m.PushBack(SEncKeyValue(key, value));
}
// ----------------------------------------------------------------------------
class CRadishDefinitionWriter {
    // ------------------------------------------------------------------------
    private var channel: CName;
    // ------------------------------------------------------------------------
    public function create(channel: CName, modcaption: String, root: SEncValue) {
        this.channel = channel;

        LogChannel(channel,
            "#-- " + modcaption + " - "
            + channel + " DUMP -------------------------------------------------");

        log("", "", root);
    }
    // ------------------------------------------------------------------------
    protected function log(indent: String, prefix: String, d: SEncValue) {
        var i: int;
        var doInlining: bool;
        var inlineList: String;
        var recursivePrefix: String;

        switch (d.type) {
            case EEVT_String:
                LogChannel(channel, indent + prefix + "\"" + d.s + "\"");
                break;

            case EEVT_Float:
                LogChannel(channel, indent + prefix + floatToStr(d.f));
                break;

            case EEVT_Bool:
                LogChannel(channel, indent + prefix + d.b);
                break;

            case EEVT_Int:
                LogChannel(channel, indent + prefix + IntToString(d.i));
                break;

            case EEVT_List:

                if (d.l.Size() <= 5) {
                    doInlining = true;
                    for (i = 0; i < d.l.Size(); i += 1) {
                        switch (d.l[i].type) {
                            case EEVT_String:
                                inlineList = inlineList + ", " + "\"" + d.l[i].s + "\"";
                                break;

                            case EEVT_Float:
                                inlineList = inlineList + ", " + floatToStr(d.l[i].f);
                                break;

                            case EEVT_Bool:
                                inlineList = inlineList + ", " + d.l[i].b;
                                break;

                            case EEVT_Int:
                                inlineList = inlineList + ", " + IntToString(d.l[i].i);
                                break;

                            default:
                                // no inlining on sub lists or sub maps
                                doInlining = false;
                                break;
                        }
                        if (!doInlining) {
                            break;
                        }
                    }
                    // remove first comma
                    inlineList = "[ " + StrRight(inlineList, StrLen(inlineList) - 2) + " ]";
                }

                if (doInlining) {
                    LogChannel(channel, indent + prefix + inlineList);
                } else {
                    LogChannel(channel, indent + prefix);
                    for (i = 0; i < d.l.Size(); i += 1) {
                        log(indent + "  ", "- ", d.l[i]);
                    }
                }

                break;

            case EEVT_Map:
                recursivePrefix = "";
                for (i = 0; i < d.m.Size(); i += 1) {
                    if (prefix != "" && prefix != "  ") {
                        recursivePrefix = "  ";
                    }
                    switch (d.m[i].v.type) {
                        // primitives inline
                        case EEVT_String:
                        case EEVT_Float:
                        case EEVT_Bool:
                        case EEVT_Int:
                            log(indent, prefix + d.m[i].k + ": ", d.m[i].v);
                            break;

                        // *sometimes* a newline for map values
                        case EEVT_List:
                            log(indent, prefix + d.m[i].k + ": ", d.m[i].v);
                            break;

                        // always a newline for map values
                        case EEVT_Map:
                            LogChannel(channel, indent + prefix + d.m[i].k + ": ");
                            log(indent + "  ", recursivePrefix, d.m[i].v);
                            break;

                        case EEVT_EmptyLine:
                            LogChannel(channel, "");
                            break;
                    }
                    prefix = recursivePrefix;
                }
                break;

            case EEVT_EmptyLine:
                LogChannel(channel, "");
                break;

            case EEVT_Null: break;

            default:
                LogChannel(channel, "ERROR: unknown value type: " + d.type);
        }
    }
    // ------------------------------------------------------------------------
    // output for encoder requires always a format with .n (1 -> 1.0)
    protected function floatToStr(f: Float) : String {
        var str: String;

        str = FloatToStringPrec(f, 10);
        if (StrFindFirst(str, ".") >= 0) {
            return str;
        }
        return str + ".0";
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
