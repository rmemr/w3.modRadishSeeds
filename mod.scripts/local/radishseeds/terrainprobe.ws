// ----------------------------------------------------------------------------
class CRadishTerrainProbe {
    var lastOpSuccess: bool;
    // ------------------------------------------------------------------------
    // since latent function cannot return result it has to be provided as
    // explicite method
    public function wasLastOpSuccessful() : bool {
        return this.lastOpSuccess;
    }
    // ------------------------------------------------------------------------
    public function refreshTerrainCollision(target: Vector) : bool {
        var tmp, groundPos, groundNormal: Vector;

        target.Z = 3000;
        target.W = 1.0;
        thePlayer.Teleport(target);

        tmp = target;
        tmp.Z = -3000;

        if (theGame.GetWorld().StaticTrace(tmp, target, groundPos, groundNormal)) {
            thePlayer.Teleport(groundPos);
            return true;
        } else {
            return false;
        }
    }
    // ------------------------------------------------------------------------
    latent function refreshTerrainCollisionWithoutBlackscreen(
        targetPos: Vector, breakOnFailure: bool, optional trackingCam: CRadishStaticCamera)
    {
        var currentPos, delta, tmp: Vector;
        var prevZ, distance: float;
        var step, steps: int;
        var camPos: SRadishPlacement;
        var success: bool;

        lastOpSuccess = false;

        currentPos = thePlayer.GetWorldPosition();
        targetPos.Z = currentPos.Z;
        prevZ = currentPos.Z;

        delta = targetPos - currentPos;
        distance = VecLength(delta);

        // break up teleport into multiple steps to interpolate and get valid
        // ground heights (10m seems to be a reliable working distance)
        // without blackscreen (camera position teleport must not exceed some max distance
        // and thus move along the path)
        if (distance > 10.0) {
            steps = CeilF(distance / 10.0);
        } else {
            steps = 1;
        }

        for (step = 0; step < steps; step += 1) {
            tmp = currentPos + delta * step / steps;
            success = this.refreshTerrainCollision(tmp);
            if (!success) {
                if (breakOnFailure) {
                    //thePlayer.Teleport(currentPos);
                    break;
                }
                // override cam height to previous height so it doesn't jump
                camPos = SRadishPlacement(tmp);
                camPos.pos.Z = prevZ;
            } else {
                camPos = SRadishPlacement(thePlayer.GetWorldPosition());
                SleepOneFrame();
            }
            // readjust cam to prevent blackscreen
            if (trackingCam) {
                prevZ = camPos.pos.Z;

                // looking from above
                camPos.pos.Z += 100;
                camPos.pos.Y -= 100;
                camPos.rot.Yaw = 0;
                camPos.rot.Pitch = -45;
                trackingCam.switchTo(camPos);
                SleepOneFrame();
            }
        }
        lastOpSuccess = success;
    }
    // ------------------------------------------------------------------------
    public function getGroundPos(
        pos: Vector, out groundPos: Vector, out groundNormal: Vector) : bool
    {
        var pos2: Vector;

        pos.W = 1.0;
        pos2 = pos;
        pos.Z = 5000;
        pos2.Z = -5000;

        return theGame.GetWorld().StaticTrace(pos, pos2, groundPos, groundNormal);
    }
    // ------------------------------------------------------------------------
    public function isWater(pos: Vector) : bool {
        var world: CWorld;
        //var waterLevel: float;
        var waterDepth: float;

        world = theGame.GetWorld();
        if (!world) {
            return false;
        }

        //waterLevel = world.GetWaterLevel(pos);
        waterDepth = world.GetWaterDepth(pos, true);

        //LogChannel('WATERTEST', "waterlevel: " + FloatToString(waterLevel) + " waterDepth: " + FloatToString(waterDepth));
        // 10000 seems to indicate there is land/no water
        return waterDepth >= 0.0 && waterDepth != 10000;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
