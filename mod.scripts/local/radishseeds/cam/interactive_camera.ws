// ----------------------------------------------------------------------------
class CRadishInteractiveCamera extends CRadishStaticCamera {
    // ------------------------------------------------------------------------
    protected var stepMoveSize: float;
    protected var stepRotSize: float;
    protected var stepGamepadSize: float; default stepGamepadSize = 20.0;
    // ------------------------------------------------------------------------
    protected var configProvider: IRadishConfigManager;
    // ------------------------------------------------------------------------
    protected var isInteractiveMode: bool;
    // ------------------------------------------------------------------------
    public function startInteractiveMode() {
        if (!isInteractiveMode) {

            refreshConfigSettings();

            // repeats & overrideExisting = true
            AddTimer('updateInteractiveSettings', 0.015f, true, , , , true);
            isInteractiveMode = true;
        }
    }
    // ------------------------------------------------------------------------
    public function stopInteractiveMode() {
        isInteractiveMode = false;
        RemoveTimer('updateInteractiveSettings');
    }
    // ------------------------------------------------------------------------
    timer function updateInteractiveSettings(deltaTime: float, id: int) {
        var newPos: Vector;
        var newRot: EulerAngles;
        var directionFB, directionLR: float;
        var moveFB, moveLR, moveUD: float;
        var rotYaw, rotPitch, rotYawPad, rotPitchPad: float;

        moveFB = theInput.GetActionValue('RAD_MoveForwardBack');
        moveLR = theInput.GetActionValue('RAD_MoveLeftRight');
        moveUD = theInput.GetActionValue('RAD_MoveUpDown');

        rotYaw = theInput.GetActionValue('GI_MouseDampX');
        rotPitch = theInput.GetActionValue('GI_MouseDampY');

        // Gamepad Support
        rotYawPad = stepGamepadSize * theInput.GetActionValue('RAD_AxisRightX');
        rotPitchPad = -stepGamepadSize * theInput.GetActionValue('RAD_AxisRightY');

        if (isInteractiveMode && (moveFB != 0 || moveLR != 0 || moveUD != 0 || rotYaw != 0 || rotPitch != 0 || rotYawPad != 0 || rotPitchPad != 0))
        {
            newPos = settings.pos;
            newRot = settings.rot;

            directionFB = Deg2Rad(GetHeading() + 90);
            directionLR = Deg2Rad(GetHeading());

            newPos.X += moveFB * stepMoveSize * CosF(directionFB)
                      + moveLR * stepMoveSize * CosF(directionLR);

            newPos.Y += moveFB * stepMoveSize * SinF(directionFB)
                      + moveLR * stepMoveSize * SinF(directionLR);

            newPos.Z += moveUD * stepMoveSize;

            newRot.Yaw -= (rotYaw + rotYawPad) * stepRotSize;
            newRot.Pitch -= (rotPitch + rotPitchPad) * stepRotSize;

            if (newPos != settings.pos || newRot != settings.rot) {
                settings.pos = newPos;
                settings.rot = newRot;

                this.applySettings(settings);
            }
        }
    }
    // ------------------------------------------------------------------------
    public function setConfig(confProvider: IRadishConfigManager) {
        configProvider = confProvider;
    }
    // ------------------------------------------------------------------------
    public function refreshConfigSettings(optional mode: String) {
        var conf: SRadishCamConfig;
        conf = configProvider.getCamConfig();
        switch (mode) {
            case "slow":
                stepMoveSize = conf.slow.stepMove;
                stepRotSize = conf.slow.stepRot;
                break;

            case "fast":
                stepMoveSize = conf.fast.stepMove;
                stepRotSize = conf.fast.stepRot;
                break;

            default:
                stepMoveSize = conf.normal.stepMove;
                stepRotSize = conf.normal.stepRot;
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
class CRadishInteractiveRotatingCamera extends CRadishInteractiveCamera {
    // ------------------------------------------------------------------------
    public var minPitch: float; default minPitch = -75.0;
    public var maxPitch: float; default maxPitch = 75.0;
    public var minDistance: float; default minDistance = 1.0;
    public var maxDistance: float; default maxDistance = 100.0;
    // ------------------------------------------------------------------------
    protected var centerPoint: Vector;
    protected var distance: float;
    // ------------------------------------------------------------------------
    public function setCenterPointSettings(center: Vector) {
        this.centerPoint = center;
        this.centerPoint.W = 1;
        this.distance = center.W;
    }
    // ------------------------------------------------------------------------
    public function getCenterPointSettings() : Vector {
        var settings: Vector;
        settings = centerPoint;
        settings.W = distance;
        return settings;
    }
    // ------------------------------------------------------------------------
    public function startInteractiveMode() {
        if (!isInteractiveMode) {

            refreshConfigSettings();

            // repeats & overrideExisting = true
            AddTimer('updateInteractiveRotatingSettings', 0.015f, true, , , , true);
            isInteractiveMode = true;
        }
    }
    // ------------------------------------------------------------------------
    public function stopInteractiveMode() {
        isInteractiveMode = false;
        RemoveTimer('updateInteractiveRotatingSettings');
    }
    // ------------------------------------------------------------------------
    timer function updateInteractiveRotatingSettings(deltaTime: float, id: int) {
        var newPos, direction: Vector;
        var newRot: EulerAngles;
        var rotYaw, rotPitch, rotYawPad, rotPitchPad: float;
        var directionFB, directionLR: float;
        var moveFB, moveLR, moveUD, moveNF: float;
        var centerPointChanged: bool;
        var offsetX, offsetY: float;

        // cam
        moveNF = theInput.GetActionValue('RAD_MouseWheel');
        rotYaw = theInput.GetActionValue('GI_MouseDampX');
        rotPitch = theInput.GetActionValue('GI_MouseDampY');
        rotYawPad = theInput.GetActionValue('RAD_AxisRightX');
        rotPitchPad = theInput.GetActionValue('RAD_AxisRightY');

        // centerpoint
        moveFB = theInput.GetActionValue('RAD_MoveForwardBack');
        moveLR = theInput.GetActionValue('RAD_MoveLeftRight');
        moveUD = theInput.GetActionValue('RAD_MoveUpDown');

        if (isInteractiveMode) {
            if (moveFB != 0 || moveLR != 0 || moveUD != 0) {

                directionFB = Deg2Rad(GetHeading() + 90);
                directionLR = Deg2Rad(GetHeading());

                offsetX = moveFB * stepMoveSize * CosF(directionFB)
                        + moveLR * stepMoveSize * CosF(directionLR);

                offsetY = moveFB * stepMoveSize * SinF(directionFB)
                        + moveLR * stepMoveSize * SinF(directionLR);

                centerPoint.X += offsetX;
                centerPoint.Y += offsetY;
                centerPoint.Z += moveUD * stepMoveSize;


                settings.pos.X += offsetX;
                settings.pos.Y += offsetY;
                settings.pos.Z += moveUD * stepMoveSize;

                centerPointChanged = true;
            }

            if (centerPointChanged || moveNF != 0 || rotYaw != 0 || rotPitch != 0 || rotYawPad != 0 || rotPitchPad != 0) {

                direction = settings.pos - centerPoint;
                newRot = VecToRotation(direction);
                distance = VecLength(direction);

                newRot.Yaw -= (rotYaw + rotYawPad) * stepRotSize;
                newRot.Pitch *= -1;
                newRot.Pitch += (rotPitch + rotPitchPad) * stepRotSize;

                distance = ClampF(distance * (1 - moveNF * stepMoveSize), minDistance, maxDistance);
                newRot.Pitch = ClampF(newRot.Pitch, minPitch, maxPitch);

                newPos = VecTransform(
                    MatrixBuiltTranslation(centerPoint) *
                    MatrixBuiltRotation(newRot),
                    Vector(0, distance, 0)
                );
                newRot = VecToRotation(centerPoint - newPos);
                newRot.Pitch *= -1;

                if (centerPointChanged || newPos != settings.pos || newRot != settings.rot) {
                    settings.pos = newPos;
                    settings.rot = newRot;

                    this.applySettings(settings);
                }
            }
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
