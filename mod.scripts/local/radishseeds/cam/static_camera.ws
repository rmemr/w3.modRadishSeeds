// ----------------------------------------------------------------------------
class CRadishStaticCamera extends CStaticCamera {
    // ------------------------------------------------------------------------
    protected var camTracker: CRadishTracker;
    // ------------------------------------------------------------------------
    protected var settings: SRadishPlacement;
    protected var useAheadOfViewTrackingPos: bool; default useAheadOfViewTrackingPos = true;
    // ------------------------------------------------------------------------
    public function setTracker(tracker: CRadishTracker) {
        this.camTracker = tracker;
    }
    // ------------------------------------------------------------------------
    public function getTracker() : CRadishTracker {
        return this.camTracker;
    }
    // ------------------------------------------------------------------------
    public function activate() {
        this.Run();
        applySettings(settings);
    }
    // ------------------------------------------------------------------------
    public function deactivate() {
        this.Stop();
    }
    // ------------------------------------------------------------------------
    public function enableAheadOfViewTrackingPosition(doEnable: bool) {
        useAheadOfViewTrackingPos = doEnable;
    }
    // ------------------------------------------------------------------------
    public function isAheadOfViewTrackingActive() : bool {
        return useAheadOfViewTrackingPos;
    }
    // ------------------------------------------------------------------------
    public function setSettings(newSettings: SRadishPlacement) {
        settings = newSettings;
    }
    // ------------------------------------------------------------------------
    public function getSettings() : SRadishPlacement {
        return settings;
    }
    // ------------------------------------------------------------------------
    public function getActiveSettings() : SRadishPlacement {
        return SRadishPlacement(this.GetWorldPosition(), this.GetWorldRotation());
    }
    // ------------------------------------------------------------------------
    public function setFov(newFov: float) {
        var comp: CCameraComponent;

        comp = (CCameraComponent)this.GetComponentByClassName('CCameraComponent');
        comp.fov = newFov;
    }
    // ------------------------------------------------------------------------
    public function getFov() : float {
        var comp: CCameraComponent;

        comp = (CCameraComponent)this.GetComponentByClassName('CCameraComponent');
        return comp.fov;
    }
    // ------------------------------------------------------------------------
    public function switchTo(optional tempSettings: SRadishPlacement) {
        var null: SRadishPlacement;

        if (!IsRunning()) {
            this.Run();
        }
        if (tempSettings != null) {
            applySettings(tempSettings);
        } else {
            applySettings(settings);
        }
    }
    // ------------------------------------------------------------------------
    protected function applySettings(settings: SRadishPlacement) {
        var pos, groundPos, groundNormal: Vector;
        var distance: Float;

        this.TeleportWithRotation(settings.pos, settings.rot);

        if (useAheadOfViewTrackingPos) {
            // adjust tracked position to a point ahead of camera
            if (this.camTracker.getTerrainProbe().getGroundPos(settings.pos, groundPos, groundNormal)) {
                distance = MaxF(5, MinF(150, AbsF(groundPos.Z - settings.pos.Z)));
            } else {
                //LogChannel('DEBUG', "camera.applySettings Z-Delta extraction failed.");
                distance = MaxF(5, MinF(100, settings.pos.Z));
            }
            pos = settings.pos + distance * VecFromHeading(settings.rot.Yaw);
            pos.Z = settings.pos.Z;
            pos.W = 1.0;
            this.camTracker.updateTrackedPos(SRadishPlacement(pos));
        } else {
            this.camTracker.updateTrackedPos(settings);
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
