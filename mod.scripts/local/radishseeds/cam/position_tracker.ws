// ----------------------------------------------------------------------------
class CRadishFoliageVisibilityTrigger extends CEntity {}
// ----------------------------------------------------------------------------
class CRadishTracker extends CEntity {
    // ------------------------------------------------------------------------
    protected var visibilityTrigger: CRadishFoliageVisibilityTrigger;
    protected var hideFoliage: bool;
    protected var hideWater: bool;
    protected var hideTerrain: bool;
    // ------------------------------------------------------------------------
    protected var terrainProbe: CRadishTerrainProbe;
    protected var terrainDimension: float;
    // ------------------------------------------------------------------------
    protected var running: bool;
    protected var trackedPlacement: SRadishPlacement;
    // ------------------------------------------------------------------------
    event OnSpawned(spawnData: SEntitySpawnData) {
        var terrainSize : float;
        var tilesCount: int;

        super.OnSpawned(spawnData);
        running = false;

        createTerrainProbe();

        if (theGame.GetWorld().GetTerrainParameters(terrainSize, tilesCount)) {
            terrainDimension = terrainSize / 2.0;
        } else {
            terrainDimension = 8000.0;
        }

        this.refreshVisibitlityTrigger();
        this.resume();
    }
    // ------------------------------------------------------------------------
    event OnDestroyed() {
        delete this.terrainProbe;
        this.visibilityTrigger.Destroy();
        this.stop();
    }
    // ------------------------------------------------------------------------
    protected function createTerrainProbe() {
        terrainProbe = new CRadishTerrainProbe in this;
    }
    // ------------------------------------------------------------------------
    public function getTerrainProbe() : CRadishTerrainProbe {
        return terrainProbe;
    }
    // ------------------------------------------------------------------------
    public function stop() {
        if (running) {
            RemoveTimer('updatePlayerPos');
            running = false;
        }
    }
    // ------------------------------------------------------------------------
    public function resume() {
        if (!running) {
            running = true;
            AddTimer('updatePlayerPos', 0.05f, true, , , , true);
        }
    }
    // ------------------------------------------------------------------------
    protected function refreshVisibitlityTrigger() {
        var template: CEntityTemplate;
        var newTrigger: CRadishFoliageVisibilityTrigger;
        var triggerComponent: CTriggerAreaEnvironmentVisibilityComponent;

        this.visibilityTrigger.Destroy();

        template = (CEntityTemplate)LoadResource("dlc\modtemplates\radishseeds\radish_foliagetrigger.w2ent", true);
        newTrigger = (CRadishFoliageVisibilityTrigger)theGame.CreateEntity(template,
            thePlayer.GetWorldPosition(), thePlayer.GetWorldRotation());

        triggerComponent = (CTriggerAreaEnvironmentVisibilityComponent)
            newTrigger.GetComponentByClassName('CTriggerAreaEnvironmentVisibilityComponent');

        triggerComponent.hideFoliage = hideFoliage;
        triggerComponent.hideWater = hideWater;
        triggerComponent.hideTerrain = hideTerrain;

        this.visibilityTrigger = newTrigger;
    }
    // ------------------------------------------------------------------------
    public function getTrackedPos() : SRadishPlacement {
        return trackedPlacement;
    }
    // ------------------------------------------------------------------------
    public function updateTrackedPos(newPos: SRadishPlacement) {
        // clamp to terrain dimensions
        newPos.pos.X = ClampF(newPos.pos.X, -terrainDimension, terrainDimension);
        newPos.pos.Y = ClampF(newPos.pos.Y, -terrainDimension, terrainDimension);
        trackedPlacement = newPos;
    }
    // ------------------------------------------------------------------------
    public function toggleTerrainVisibility() : bool {
        this.hideTerrain = !this.hideTerrain;
        this.refreshVisibitlityTrigger();
        return !this.hideTerrain;
    }
    // ------------------------------------------------------------------------
    public function toggleFoliageVisibility() : bool {
        this.hideFoliage = !this.hideFoliage;
        this.refreshVisibitlityTrigger();
        return !this.hideFoliage;
    }
    // ------------------------------------------------------------------------
    public function toggleWaterVisibility() : bool {
        this.hideWater = !this.hideWater;
        this.refreshVisibitlityTrigger();
        return !this.hideWater;
    }
    // ------------------------------------------------------------------------
    private function showVisibilityNotice(type: String, isOn: bool) {
        var visibility: String;

        if (isOn) {
            visibility = "RAD_iVisibilityOn";
        } else {
            visibility = "RAD_iVisibilityOff";
        }
        theGame.GetGuiManager().ShowNotification(
            GetLocStringByKeyExt(type) + GetLocStringByKeyExt(visibility));
    }
    // ------------------------------------------------------------------------
    public function toggleTerrainVisibilityWithInfo() : bool {
        var isOn: bool;
        isOn = toggleTerrainVisibility();
        showVisibilityNotice("RAD_iTerrainVisibility", isOn);
        return isOn;
    }
    // ------------------------------------------------------------------------
    public function toggleFoliageVisibilityWithInfo() : bool {
        var isOn: bool;
        isOn = toggleFoliageVisibility();
        showVisibilityNotice("RAD_iFoliageVisibility", isOn);
        return isOn;
    }
    // ------------------------------------------------------------------------
    public function toggleWaterVisibilityWithInfo() : bool {
        var isOn: bool;
        isOn = toggleWaterVisibility();
        showVisibilityNotice("RAD_iWaterVisibility", isOn);
        return isOn;
    }
    // ------------------------------------------------------------------------
    timer function updatePlayerPos(deltaTime: float, id: int) {
        var prevPos, playerPos, trackedPos, tmpPos, delta: Vector;
        var distance: Float;

        if (trackedPlacement.pos != playerPos) {
            trackedPos = trackedPlacement.pos;
            prevPos = thePlayer.GetWorldPosition();
            playerPos = prevPos;
            // ignore height difference!
            playerPos.Z = 0;
            trackedPos.Z = 0;

            delta = trackedPos - playerPos;
            distance = VecLength(delta);

            if (distance > 5.0) {

                // break up teleport into multiple steps to interpolate and get valid
                // ground heights (10m seems to be a reliable working distance)
                // without blackscreen
                if (distance > 10.0) {
                    tmpPos = playerPos + delta * 1 / CeilF(distance / 10.0);
                    if (terrainProbe.refreshTerrainCollision(tmpPos)) {
                        visibilityTrigger.Teleport(tmpPos);
                        return;
                    }
                } else {
                    if (terrainProbe.refreshTerrainCollision(trackedPos)) {
                        visibilityTrigger.Teleport(trackedPos);
                        return;
                    }
                }
            }
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
