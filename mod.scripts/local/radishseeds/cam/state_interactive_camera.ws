// ----------------------------------------------------------------------------
abstract state Rad_InteractiveCamera in IScriptable {
    // ------------------------------------------------------------------------
    protected var theCam: CRadishInteractiveCamera;
    protected var workContext: CName; default workContext = 'MOD_Radish_InteractiveCam';
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        theCam = createCam();
        theCam.activate();
        theCam.startInteractiveMode();

        theInput.StoreContext(workContext);
        registerListeners();
    }
    // ------------------------------------------------------------------------
    event OnLeaveState(nextStateName: CName) {
        theCam.stopInteractiveMode();
        theCam.Destroy();
        delete theCam;

        unregisterListeners();
        theInput.RestoreContext(workContext, true);
    }
    // ------------------------------------------------------------------------
    event OnBack(action: SInputAction) {
        if (IsReleased(action)) {
            // immediately stop interactive movement to prevent weird behaviour with
            // view opening of previous state (no idea what the root cause is...)
            theCam.stopInteractiveMode();
            backToPreviousState(action);
        }
    }
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {
        hotkeyList.PushBack(HotkeyHelp_from('RAD_ToggleInteractiveCam'));

        hotkeyList.PushBack(HotkeyHelp_from('GI_MouseDampX', "RAD_CamRotYaw"));
        hotkeyList.PushBack(HotkeyHelp_from('GI_MouseDampY', "RAD_CamRotPitch"));

        hotkeyList.PushBack(HotkeyHelp_from('RAD_MoveForwardBack', "RAD_CamMoveForwardBack"));
        hotkeyList.PushBack(HotkeyHelp_from('RAD_MoveLeftRight', "RAD_CamMoveLeftRight"));
        hotkeyList.PushBack(HotkeyHelp_from('RAD_MoveUpDown', "RAD_CamMoveUpDown"));
        hotkeyList.PushBack(HotkeyHelp_from('RAD_ToggleFast'));
        hotkeyList.PushBack(HotkeyHelp_from('RAD_ToggleSlow'));
        hotkeyList.PushBack(HotkeyHelp_from('RAD_Back', "RAD_StopInteractiveCam"));
    }
    // ------------------------------------------------------------------------
    event OnChangeSpeed(action: SInputAction) {
        if (IsPressed(action)) {
            if (action.aName == 'RAD_ToggleFast') {
                theCam.refreshConfigSettings("fast");
            } else {
                theCam.refreshConfigSettings("slow");
            }
        } else if (IsReleased(action)) {
            theCam.refreshConfigSettings("normal");
        }
    }
    // ------------------------------------------------------------------------
    protected function backToPreviousState(action: SInputAction);
    protected function createCam() : CRadishInteractiveCamera;
    // ------------------------------------------------------------------------
    protected function registerListeners() {
        theInput.RegisterListener(this, 'OnBack', 'RAD_Back');

        theInput.RegisterListener(this, 'OnChangeSpeed', 'RAD_ToggleFast');
        theInput.RegisterListener(this, 'OnChangeSpeed', 'RAD_ToggleSlow');
    }
    // ------------------------------------------------------------------------
    protected function unregisterListeners() {
        theInput.UnregisterListener(this, 'RAD_Back');

        theInput.UnregisterListener(this, 'RAD_ToggleFast');
        theInput.UnregisterListener(this, 'RAD_ToggleSlow');
    }
    // ------------------------------------------------------------------------
    protected function createAndSetupInteractiveCam(
        config: IRadishConfigManager, placement: SRadishPlacement, camTracker: CRadishTracker) : CRadishInteractiveCamera
    {
        var cam: CRadishInteractiveCamera;
        var template: CEntityTemplate;

        template = (CEntityTemplate)LoadResource("dlc\modtemplates\radishseeds\interactive_camera.w2ent", true);
        cam = (CRadishInteractiveCamera)theGame.CreateEntity(template, placement.pos, placement.rot);

        cam.setTracker(camTracker);
        cam.setConfig(config);
        cam.setSettings(placement);
        return cam;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
abstract state Rad_InteractiveRotatingCamera in IScriptable extends Rad_InteractiveCamera {
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {
        hotkeyList.PushBack(HotkeyHelp_from('RAD_ToggleInteractiveCam'));

        hotkeyList.PushBack(HotkeyHelp_from('GI_MouseDampX', "RAD_CamRotYaw"));
        hotkeyList.PushBack(HotkeyHelp_from('GI_MouseDampY', "RAD_CamRotPitch"));
        hotkeyList.PushBack(HotkeyHelp_from('RAD_MouseWheel', "RAD_CamMoveForwardBack"));

        hotkeyList.PushBack(HotkeyHelp_from('RAD_MoveForwardBack', "RAD_CenterMoveForwardBack"));
        hotkeyList.PushBack(HotkeyHelp_from('RAD_MoveLeftRight', "RAD_CenterMoveLeftRight"));
        hotkeyList.PushBack(HotkeyHelp_from('RAD_MoveUpDown', "RAD_CenterMoveUpDown"));
        hotkeyList.PushBack(HotkeyHelp_from('RAD_ToggleFast'));
        hotkeyList.PushBack(HotkeyHelp_from('RAD_ToggleSlow'));
        hotkeyList.PushBack(HotkeyHelp_from('RAD_Back', "RAD_StopInteractiveCam"));
    }
    // ------------------------------------------------------------------------
    protected function createAndSetupInteractiveCam(
        config: IRadishConfigManager, placement: SRadishPlacement, camTracker: CRadishTracker) : CRadishInteractiveCamera
    {
        var cam: CRadishInteractiveRotatingCamera;
        var template: CEntityTemplate;

        template = (CEntityTemplate)LoadResource("dlc\modtemplates\radishseeds\interactive_rotating_camera.w2ent", true);
        cam = (CRadishInteractiveRotatingCamera)theGame.CreateEntity(template, placement.pos, placement.rot);

        cam.setTracker(camTracker);
        cam.setConfig(config);
        cam.setSettings(placement);
        // doesn't make sense for rotating camera
        cam.enableAheadOfViewTrackingPosition(false);
        return cam;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
