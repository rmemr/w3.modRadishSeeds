// ----------------------------------------------------------------------------
// DO NOT CHANGE PROPERTY ORDER (important for initialization!)
// ----------------------------------------------------------------------------
struct SRadishInteractiveStepConfig {
    var stepMove: float;
    var stepRot: float;
}
// ----------------------------------------------------------------------------
struct SRadishCamConfig {
    var slow: SRadishInteractiveStepConfig;
    var normal: SRadishInteractiveStepConfig;
    var fast: SRadishInteractiveStepConfig;
    // -- auto saved usage based
    // volatile: updated/read while normal usage
    var switchOnSelect: bool;
    // static: read on start, updated on exit
    var lastPos: SRadishPlacement;
}
// ----------------------------------------------------------------------------
struct SRadishPlacementConfig {
    var slow: SRadishInteractiveStepConfig;
    var normal: SRadishInteractiveStepConfig;
    var fast: SRadishInteractiveStepConfig;
}
// ----------------------------------------------------------------------------
