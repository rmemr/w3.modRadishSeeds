// ----------------------------------------------------------------------------
abstract state Rad_InteractiveTime in IScriptable {
    // ------------------------------------------------------------------------
    protected var workContext: CName; default workContext = 'MOD_Radish_InteractiveTime';
    // ------------------------------------------------------------------------
    protected var stepSizeFast: int; default stepSizeFast = 60;
    protected var stepSizeNormal: int; default stepSizeNormal = 30;
    protected var stepSizeSlow: int; default stepSizeSlow = 1;
    // ------------------------------------------------------------------------
    protected var stepSize: int;
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        stepSize = stepSizeNormal;

        theInput.StoreContext(workContext);
        registerListeners();
    }
    // ------------------------------------------------------------------------
    event OnLeaveState(nextStateName: CName) {
        unregisterListeners();
        theInput.RestoreContext(workContext, true);
    }
    // ------------------------------------------------------------------------
    event OnBack(action: SInputAction) {
        if (IsReleased(action)) {
            backToPreviousState(action);
        }
    }
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {
        hotkeyList.PushBack(HotkeyHelp_from('RAD_AdjustTime'));
        hotkeyList.PushBack(HotkeyHelp_from('RAD_ToggleSlow', "RAD_ToggleSlowTime"));
        hotkeyList.PushBack(HotkeyHelp_from('RAD_ToggleFast', "RAD_ToggleFastTime"));
        hotkeyList.PushBack(HotkeyHelp_from('RAD_Back', "RAD_StopInteractiveTime", IK_Escape));
    }
    // ------------------------------------------------------------------------
    event OnChangeSpeed(action: SInputAction) {
        if (IsPressed(action)) {
            if (action.aName == 'RAD_ToggleFast') {
                stepSize = stepSizeFast;
            } else {
                stepSize = stepSizeSlow;
            }
        } else if (IsReleased(action)) {
            stepSize = stepSizeNormal;
        }
    }
    // ------------------------------------------------------------------------
    event OnChangeTime(action: SInputAction) {
        var days, minSec, maxSec, newTime: int;
        var gameTime: GameTime;

        if (action.value != 0) {
            gameTime = theGame.GetGameTime();
            days = GameTimeDays(gameTime);
            minSec = GameTimeToSeconds(GameTimeCreate(days, 0, 0, 0));
            maxSec = GameTimeToSeconds(GameTimeCreate(days, 23, 59, 59));

            newTime = GameTimeToSeconds(gameTime) + (int)action.value * stepSize *60;

            // wrap around
            if (newTime > maxSec) {
                newTime -= 86400;
            } else if (newTime < minSec) {
                newTime += 86400;
            }

            gameTime = GameTimeCreateFromGameSeconds(newTime);
            theGame.SetGameTime(gameTime, true);
        }
    }
    // ------------------------------------------------------------------------
    protected function getTimeCaption() : String {
        var gameTime: GameTime;
        var hours, mins: int;
        var h, m: String;

        gameTime = theGame.GetGameTime();
        hours = GameTimeHours(gameTime);
        mins = GameTimeMinutes(gameTime);

        if (hours < 10) { h = "0" + IntToString(hours); } else { h = IntToString(hours); }
        if (mins < 10) { m = "0" + IntToString(mins); } else { m = IntToString(mins); }

        return h + ":" + m;
    }
    // ------------------------------------------------------------------------
    protected function backToPreviousState(action: SInputAction);
    // ------------------------------------------------------------------------
    protected function registerListeners() {
        theInput.RegisterListener(this, 'OnBack', 'RAD_Back');

        theInput.RegisterListener(this, 'OnChangeTime', 'RAD_AdjustTime');
        theInput.RegisterListener(this, 'OnChangeSpeed', 'RAD_ToggleFast');
        theInput.RegisterListener(this, 'OnChangeSpeed', 'RAD_ToggleSlow');
    }
    // ------------------------------------------------------------------------
    protected function unregisterListeners() {
        theInput.UnregisterListener(this, 'RAD_Back');

        theInput.UnregisterListener(this, 'RAD_AdjustTime');
        theInput.UnregisterListener(this, 'RAD_ToggleFast');
        theInput.UnregisterListener(this, 'RAD_ToggleSlow');
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
