// ----------------------------------------------------------------------------
// DO NOT CHANGE PROPERTY ORDER (important for initialization!)
// ----------------------------------------------------------------------------
struct SRadishPlacement {
    var pos: Vector;
    var rot: EulerAngles;
}
// ----------------------------------------------------------------------------
struct SRadishRect {
    var x1: int;
    var y1: int;
    var x2: int;
    var y2: int;
}
// ----------------------------------------------------------------------------
