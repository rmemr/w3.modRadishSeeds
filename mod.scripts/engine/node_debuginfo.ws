// ----------------------------------------------------------------------------
struct SDbgInfo {
    var type: String;
    var n: CName;
    var s: String;
    var i: int;
    var f: float;
    var v: array<SDbgInfo>;
}
// ----------------------------------------------------------------------------
